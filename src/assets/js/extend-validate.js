import { validators } from 'validate.js';

validators.currency = function (value, options, key, attributes) {
    if (Number(value) === 0)
        return 'can not be 0';
        
    const regex = '^\\d+(?:\\.\\d{1,2})?$';
    return new RegExp(regex).test(value) ? null : 'is not correct currency format.';
};