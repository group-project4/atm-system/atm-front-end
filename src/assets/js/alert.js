import $ from 'jquery';

export const AlertType = Object.freeze({
    WARNING: 'warning',
    DANGER: 'danger',
    SUCCESS: 'success',
    INFO: 'info'
});

export function AlertMessage({message, alertType}) {
    this.alertMessage = message;
    this.alertType = alertType;
    this.show = function() {
        // define icon type
        let icon = '';

        switch (alertType) {
            case AlertType.DANGER:
                icon = '<i class="fas fa-exclamation-triangle"></i>';
                break;
            
            case AlertType.SUCCESS:
                icon = '<i class="far fa-check-circle"></i>';
                break;

            case AlertType.WARNING:
                icon = '<i class="fas fa-exclamation"></i>';
                break;

            default:
                break;
        }

        const alertHTML = (`
            <div class="system-alert alert alert-${this.alertType} alert-dismissible fade show" role="alert">
                <strong>
                    <span class="mr-1">${icon}</span>
                    ${this.alertMessage}
                </strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        `);

        const alertElement = new DOMParser()
            .parseFromString(alertHTML, 'text/html')
            .getElementsByTagName('div')[0];

        document.getElementsByTagName('body')[0].append(alertElement);

        // auto close alert after 5seconds.
        
        setTimeout(() => {
            $(alertElement).alert('close');
        }, 5000);
        
    }
}