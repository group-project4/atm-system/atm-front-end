export function showLoading() {
    const loadingElement = document.getElementById('systemLoading');
    const rootElement = document.getElementById('root');

    rootElement.style.opacity = '0.8';
    rootElement.style.pointerEvents = 'none';

    loadingElement.classList.remove('d-none');
    loadingElement.classList.add('d-block');
}

export function hideLoading() {
    const loadingElement = document.getElementById('systemLoading');
    const rootElement = document.getElementById('root');

    rootElement.style.opacity = '1';
    rootElement.style.pointerEvents = 'auto';

    loadingElement.classList.remove('d-block');
    loadingElement.classList.add('d-none');
}