import React from 'react';
import ReactDOM from 'react-dom';
import 'jquery';
import 'popper.js';
import './assets/js/extend-validate';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import './assets/css/global.css';
import './assets/css/all.min.css';
import App from './components/app/app.component';
import store from './redux/store';
import { Provider } from 'react-redux';

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>, 
    document.getElementById('root')
);

