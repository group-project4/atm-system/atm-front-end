import { useSelector } from "react-redux";

export function useAPIHeader() {
    const { token } = useSelector((state) => state.authReducer);
    
    return {
        Authorization: `Bearer ${token}`
    }
}