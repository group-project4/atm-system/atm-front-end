import { useRef, useCallback, useState } from 'react';
import { validate } from 'validate.js';

export function useFormValidation(constraint) {

    const [form, setForm] = useState(null);

    function validateForm() {
        if (!form)
            return;

        const allFields = Object.keys(constraint);
        const fieldToValidate = {};
        // query input fields
        allFields.forEach((fieldName) => {
            const inputField = form.querySelector(`#${fieldName}`);
            if (!inputField) throw new Error('Field does not exist');
            Object.defineProperty(fieldToValidate, fieldName, {
                value: inputField.value,
                writable: false
            });

        });

        const errorsValidation = validate(fieldToValidate, constraint);

        const errorMessages = form.querySelectorAll('.field-error');
        if (errorMessages.length) errorMessages.forEach((element) => element.remove());

        if (errorsValidation) {
            const errorKeys = Object.entries(errorsValidation);

            errorKeys.forEach((keyValuePaire) => {
                const keyName = keyValuePaire[0];
                const errorMessages = keyValuePaire[1]

                // add error message for invalid input field
                const errorElement = document.createElement('span');
                errorElement.classList.add('field-error');
                errorElement.textContent = errorMessages[0]; // show first error.

                const inputField = form.querySelector(`#${keyName}`);
                if (!inputField) throw new Error('Field does not exist');
                inputField.after(errorElement);
            });

            return false;
        }

        return true;
    }

    return [setForm, validateForm];
}