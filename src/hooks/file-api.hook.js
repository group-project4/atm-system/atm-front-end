import { useAPI } from './api.hook';

export function useConvertToAPIFile() {
    const APIEndPoint = useAPI();
    return function(imageName) {
        return `${APIEndPoint.BASE_END_POINT}/static/${imageName}`;
    }
}