import enLang from './../assets/language/en.json'
import khLang from './../assets/language/kh.json';

const en = enLang;
const kh = khLang;

export function useLanguageTranslate(lang) {
    if (lang === 'en') 
        return new LanguageTranslator(en);
    if (lang === 'kh')
        return new LanguageTranslator(kh);
}

function LanguageTranslator(langFile) {
    this.langFile = langFile;
    this.translate = function(langKey) {
        return this.langFile[langKey];
    }
}