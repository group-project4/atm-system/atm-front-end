import { AlertMessage, AlertType } from './../assets/js/alert';

export function useAxiosErrorHandler() {

    function handleAxiosRequestError(error) {
        if (!error.response) {
            new AlertMessage({
                alertType: AlertType.DANGER,
                message: 'Please check internet connection'
            }).show();
        } else if (error.response.status === 500) {
            new AlertMessage({
                alertType: AlertType.DANGER,
                message: 'Server Error'
            }).show();
        } else if (error.response.status === 400) {
            new AlertMessage({
                alertType: AlertType.DANGER,
                message: 'Not enough data to send to server.'
            }).show();
        } else if (error.response.status === 401) {
            new AlertMessage({
                alertType: AlertType.DANGER,
                message: 'Permission denied'
            }).show();
        } else if (error.response.status === 409) {
            new AlertMessage({
                alertType: AlertType.DANGER,
                message: error.response.data.message
            }).show();
        }
    }

    return handleAxiosRequestError;
}