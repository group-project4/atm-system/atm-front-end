const { useSelector } = require("react-redux");

export function useCheckAuth() {
    const { token, username, role } = useSelector((state) => state.authReducer);

    return token && username && role;
}