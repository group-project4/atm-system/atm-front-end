export function useAPI() {
    const BASE_END_POINT = 'http://localhost:8000';

    return Object.freeze({
        BASE_END_POINT: BASE_END_POINT,
        CREATE_USER: `${BASE_END_POINT}/api/v1.0/user/create`,
        GET_USERS: `${BASE_END_POINT}/api/v1.0/user`,
        GET_BILLING_TRANSACTION: `${BASE_END_POINT}/api/v1.0/billing`,
        GET_DEPOIST_TRANSACTION: `${BASE_END_POINT}/api/v1.0/deposit`,
        GET_WITHDRAWAL_TRANSACTION: `${BASE_END_POINT}/api/v1.0/withdraw`,
        GET_TRANSFER_TRANSACTION: `${BASE_END_POINT}/api/v1.0/transfer`,
        GET_TOPUP_TRANSACTION: `${BASE_END_POINT}/api/v1.0/phone-topup`,
        GET_BILLER_TYPES: `${BASE_END_POINT}/api/v1.0/biller-type`,
        GET_BILLER_BY_BILLER_TYPE: `${BASE_END_POINT}/api/v1.0/biller/biller-type`,
        GET_BILLER_BY_BILLER_NAME: `${BASE_END_POINT}/api/v1.0/biller/biller-type/name`,
        GET_ACCOUNT: `${BASE_END_POINT}/api/v1.0/user/account`,
        CREATE_BILLER_TYPE: `${BASE_END_POINT}/api/v1.0/biller-type/create`,
        LOGIN: `${BASE_END_POINT}/api/v1.0/auth/login`,
        CREATE_BILLER: `${BASE_END_POINT}/api/v1.0/biller/create`,
        GET_BILLERS: `${BASE_END_POINT}/api/v1.0/biller`,
        GET_CURRENT_USER: `${BASE_END_POINT}/api/v1.0/user/current`,
        DEPOSIT: `${BASE_END_POINT}/api/v1.0/deposit/create`,
        WITHDRAW: `${BASE_END_POINT}/api/v1.0/withdraw/create`,
        TRANSFER: `${BASE_END_POINT}/api/v1.0/transfer/create`,
        PAY_BILL: `${BASE_END_POINT}/api/v1.0/billing/create`,
        TOPUP: `${BASE_END_POINT}/api/v1.0/phone-topup/create`,
        CHANGE_PASSWORD: `${BASE_END_POINT}/api/v1.0/user/password/change`,
        UPDATE_USER_STATUS: `${BASE_END_POINT}/api/v1.0/user/status/update`,
        UPLOAD_FILE: `${BASE_END_POINT}/api/v1.0/file/upload`
    });
}