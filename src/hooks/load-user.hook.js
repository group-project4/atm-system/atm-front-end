export function useLoadUserData() {
    const data = localStorage.getItem('ud');

    if (!data)
        return null;
    
    const userData = atob(atob(localStorage.getItem('ud')));

    try {
        return JSON.parse(userData);
    } catch (error) {
        return null;
    }
}