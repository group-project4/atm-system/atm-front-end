export function useDollarToCentConverter() {
    return (dollar) => parseFloat(dollar) * 100;
}

export function useCentToDollarConverter() {
    return (cent) => parseInt(cent) / 100;
}