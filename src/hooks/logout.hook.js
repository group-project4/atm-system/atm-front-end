import { useDispatch } from "react-redux";
import { AuthActionType } from './../redux/reducers/auth.reducer';

export function useLogout() {
    const dispatch = useDispatch();

    function logout() {
        // remove auth information from storage and redux
        localStorage.clear('ud');
        dispatch({
            type: AuthActionType.CLEAR
        });
    }

    return logout;
}