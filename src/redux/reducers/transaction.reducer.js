export const TransactionActionType = Object.freeze({
    HAS_TRANSACTION: 'HAS_TRANSACTION'
});

const defaultState = {
    transactionBalance: 0
};

export function transactionReducer(state=defaultState, action) {
    const newState = {...state};

    switch (action.type) {
        case TransactionActionType.HAS_TRANSACTION:
            newState.transactionBalance = action.payload.transactionBalance;
            break;
    
        default:
            break;
    }

    return newState;
}