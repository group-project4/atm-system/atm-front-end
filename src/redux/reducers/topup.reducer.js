const defaultState = {
    companyId: null,
    topupBalance: null
};

export const TopupActionType = {
    SELECT_COMPANY: 'SELECT_COMPANY',
    SELECT_BALANCE: 'SELECT_BALANCE',
    RESET_VALUE: 'RESET_VALUE'
};

export function topupReducer(state = defaultState, action) {
    const newState = {...state};
    switch (action.type) {
        case TopupActionType.SELECT_BALANCE:
            const {balance} = action.payload;
            newState.topupBalance = balance;
            break;
        case TopupActionType.SELECT_COMPANY:
            const {companyId} = action.payload;
            newState.companyId = companyId;
            break;
        case TopupActionType.RESET_VALUE:
            newState.companyId = null;
            newState.topupBalance = null;
            break;
    }
    return newState;
}