export const AuthActionType = Object.freeze({
    SAVE: 'SAVE_USER_INFORMATION',
    LOAD: 'LOAD_USER_INFORMATION',
    CLEAR: 'CLEAR_USER_INFORMATION'
});

const defaultState = {
    username: '',
    role: '',
    token: ''
};

export function authReducer(state=defaultState, action) {

    const newState = {...state};

    switch (action.type) {
        case AuthActionType.SAVE:
            const payload = action.payload;
            const username = payload.username;
            const role = payload.role;
            const token = payload.token;

            const userData = {
                username,
                role,
                token
            };

            const encoded = btoa(btoa(JSON.stringify(userData)));

            localStorage.setItem('ud', encoded);

            newState.username = username;
            newState.role = role;
            newState.token = token;
            
            break;

        case AuthActionType.LOAD:
            newState.username = action.payload.username;;
            newState.role = action.payload.role;
            newState.token = action.payload.token;

            break;
            
        case AuthActionType.CLEAR:
            newState.username = '';
            newState.role = '';
            newState.token = ''
            
            break;

        default:
            break;
    }

    return newState;
}