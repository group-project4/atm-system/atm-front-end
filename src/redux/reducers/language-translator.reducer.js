export const LanguageTranslatorActionType = Object.freeze({
    CHANGE_LANGUAGE: 'CHANGE_LANGUAGE'
});

const defaultState = {
    lang: 'en'
};

export function languageTranslatorReducer(state=defaultState, action) {
    const newState = {...state};

    switch (action.type) {
        case LanguageTranslatorActionType.CHANGE_LANGUAGE:
            const { lang } = action.payload;
            newState.lang = lang;
            break;
    
        default:
            break;
    }

    return newState;
}