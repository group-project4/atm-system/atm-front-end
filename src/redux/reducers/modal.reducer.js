const defaultState = {
    form: null,
    title: null,
    isModalClosed: true
};

export const ModalActionType = {
    OPEN_MODAL: 'OPEN_MODAL',
    CLOSE_MODAL: 'CLOSE_MODAL'
};

export function modalReducer(state = defaultState, action) {
    const newState = {...state};
    switch (action.type) {
        case ModalActionType.OPEN_MODAL:
            const {form, title} = action.payload;
            newState.form = form;
            newState.title = title;
            newState.isModalClosed = false;
            break;
        case ModalActionType.CLOSE_MODAL:
            newState.form = null;
            newState.title = null;
            newState.isModalClosed = true;
            break;
    }
    return newState;
}