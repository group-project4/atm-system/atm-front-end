import { createStore, combineReducers } from 'redux';
import { modalReducer } from './reducers/modal.reducer';
import { topupReducer } from './../redux/reducers/topup.reducer';
import { authReducer } from './../redux/reducers/auth.reducer';
import { transactionReducer } from './../redux/reducers/transaction.reducer';
import { languageTranslatorReducer } from './../redux/reducers/language-translator.reducer';

const rootReducer = combineReducers({
    modalReducer,
    topupReducer,
    authReducer,
    transactionReducer,
    languageTranslatorReducer
});

const store = createStore(rootReducer);

export default store;