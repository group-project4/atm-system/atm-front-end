import React from 'react';
import { useSelector } from "react-redux";
import { Redirect, Route, useLocation } from "react-router-dom";
import {
    useCheckAuth
} from '../../hooks';

export function AuthRoute({component, path, ...rest}) {
    const isLoggedIn = useCheckAuth();
    const { pathname } = useLocation();
    const { role } = useSelector((state) => state.authReducer);
    
    if (!isLoggedIn) {
        return (
            <Redirect to="/login" />
        )
    }

    // user has role customer but try to access to admin path through browser directly.
    if (pathname.toLowerCase().includes('admin') && role === 'normal') {
        return <Redirect to="/customer" />
    } else if (pathname.toLowerCase().includes('customer') && role ==='admin') {
        return <Redirect to="/admin" />
    }

    return (
        <Route path={path} component={component} />
    )
}