import React, { useEffect, useState, useReducer } from 'react';
import './customer-dashboard.style.css';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { ModalActionType } from '../../redux/reducers/modal.reducer';
import { TransactionActionType } from '../../redux/reducers/transaction.reducer';
import FormDeposit from '../form/deposit/form-deposit.component';
import FormWithdrawal from '../form/withdrawal/form-withdrawal.component';
import FormTransfer from '../form/transfer/form-transfer.component';
import FormTopUp from '../form/top-up/form-top-up.component';
import FormChangePassword from '../form/change-password/change-password.component';
import FormBillPayment from '../form/bill-payment/form-bill-payment.component';
import { useLogout } from './../../hooks/logout.hook';
import { useAPIHeader } from './../../hooks/api-header.hook';
import { useAPI } from './../../hooks/api.hook';
import Axios from 'axios';
import { useAxiosErrorHandler } from './../../hooks/axios-error-handler.hook';
import { AlertMessage, AlertType } from './../../assets/js/alert';
import { hideLoading, showLoading } from './../../assets/js/loading';
import { useCentToDollarConverter } from './../../hooks/balance-converter.hook';
import { useLanguageTranslate } from '../../hooks';
import { LanguageTranslatorActionType } from './../../redux/reducers/language-translator.reducer';
import AccountInformation from '../form/account-info/account-info.component';
import moment from 'moment';

function CustomerDashboard() {
    const [currentUser, setCurrentUser] = useState(null);
    const [accountName, setAccountName] = useState('N/A');
    const [balance, setBalance] = useState('N/A');
    
    const { transactionBalance } = useSelector((state) => state.transactionReducer);
    const { lang } = useSelector((state) => state.languageTranslatorReducer);
    
    const language = useLanguageTranslate(lang);
    const history = useHistory();
    const dispatch = useDispatch();
    const logout = useLogout();
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();
    const centToDollar = useCentToDollarConverter();

    function onButtonExitClick(evt) {
        logout();
        history.push('/');
    }

    function getUserAccountInfo() {
        showLoading();
        Axios.get(APIEndPoint.GET_CURRENT_USER, {
            headers: apiHeader
        })
        .then((response) => {
            return response.data;
        })
        .then((jsonResponse) => {
            setCurrentUser(jsonResponse.data);
        })
        .catch((error) => {
            handleAxiosError(error);
        })
        .finally(() => {
            hideLoading();
        });
    }

    function changeLanguage(lang) {
        dispatch({
            type: LanguageTranslatorActionType.CHANGE_LANGUAGE,
            payload: {
                lang: lang
            }
        });
    }

    useEffect(() => {
        getUserAccountInfo();
    }, []);

    useEffect(() => {
        if (!currentUser) return;

        setAccountName(currentUser.account.account_name);
        setBalance(centToDollar(currentUser.account.balance));

    }, [currentUser]);

    useEffect(() => {
        if (!transactionBalance) return;

        // update user balance for each transaction.
        const updatedBalance = Number(balance) + centToDollar(transactionBalance);
        setBalance(updatedBalance);
    }, [transactionBalance]);

    return (
        <div className="customer-dashboard">
            <div className="top">
                <div className="row">
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-flex justify-content-start align-items-center">
                        <h1 className="atm-title m-0 d-flex align-items-center en">ATM</h1>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <button className="btn-exit float-right shadow-sm" type="button" onClick={onButtonExitClick}>
                            <span className={lang}>{language.translate('exit')}</span>
                        </button>
                    </div>
                </div>
                <div className="row" style={{marginTop: '5em'}}>
                    <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <div className="d-flex flex-column">
                            <div>
                                <h3 className="font-weight-bold" style={{color: '#ffffff'}}>
                                    <span className={lang}>{language.translate('welcome')}</span>
                                </h3>
                                <h4 style={{color: '#dfe4ea'}} className="en">{accountName}</h4>
                            </div>
                            <div className="mt-5">
                                <h3 className="font-weight-bold" style={{color: '#ffffff'}}>
                                    <span className={lang}>{language.translate('account')}</span>
                                </h3>
                                <h4 style={{color: '#dfe4ea'}}>
                                    <span className={lang}>{language.translate('currency')}</span> 
                                    <span className={balance === 0 ? 'text-danger en' : 'en'}> {balance}</span>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                        <div className="row">
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow" 
                                    onClick={(evt) => dispatch({
                                        type: ModalActionType.OPEN_MODAL,
                                        payload: {
                                            form: <FormDeposit />,
                                            title: 'Deposit'
                                        }
                                    })}
                                >
                                    <span className={lang}>{language.translate('deposit')}</span>
                                </button>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow"
                                    onClick={(evt) => dispatch({
                                        type: ModalActionType.OPEN_MODAL,
                                        payload: {
                                            form: <FormWithdrawal />,
                                            title: 'Withdrawal'
                                        }
                                    })}
                                >
                                    <span className={lang}>{language.translate('withdraw')}</span>
                                </button>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow"
                                    onClick={(evt) => dispatch({
                                        type: ModalActionType.OPEN_MODAL,
                                        payload: {
                                            form: <FormTransfer />,
                                            title: 'Transfer'
                                        }
                                    })}
                                >
                                    <span className={lang}>{language.translate('transfer')}</span>
                                </button>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow"
                                    onClick={(evt) => {
                                        dispatch({
                                            type: ModalActionType.OPEN_MODAL,
                                            payload: {
                                                form: <FormBillPayment />,
                                                title: 'Bill Payment'
                                            }
                                        });
                                    }}
                                >
                                    <span className={lang}>{language.translate('billPayment')}</span>
                                </button>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow"
                                    onClick={(evt) => dispatch({
                                        type: ModalActionType.OPEN_MODAL,
                                        payload: {
                                            form: <FormTopUp />,
                                            title: 'Topup'
                                        }
                                    })}
                                >
                                    <span className={lang}>{language.translate('topup')}</span>
                                </button>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow"
                                    onClick={(evt) => {
                                        dispatch({
                                            type: ModalActionType.OPEN_MODAL,
                                            payload: {
                                                title: 'Change Password',
                                                form: <FormChangePassword />
                                            }
                                        });
                                    }}
                                >
                                    <span className={lang}>{language.translate('changePassword')}</span>
                                </button>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow" disabled>
                                    <span className={lang}>{language.translate('report')}</span>
                                </button>
                            </div>
                            <div className="col-lg-6 col-md-12 col-12">
                                <button type="button" className="btn-function shadow"
                                    onClick={(evt) => {
                                        dispatch({
                                            type: ModalActionType.OPEN_MODAL,
                                            payload: {
                                                title: 'Account Information',
                                                form: <AccountInformation  
                                                            accountName={currentUser.account.account_name}
                                                            accountNumber={currentUser.account.account_id}
                                                            phoneNumber={currentUser.user_phone}
                                                            balance={balance}
                                                            registrationDate={moment(currentUser.created_date).format('DD-MM-YYYY')}
                                                        />
                                            }
                                        });
                                    }}
                                >
                                    <span className={lang}>{language.translate('accinfo')}</span>
                                </button>
                                <div className="btn-group btn-group-sm float-right" role="group">
                                    <button className="btn btn-outline-primary" onClick={(evt) => changeLanguage('en')}>EN</button>
                                    <button className="btn btn-outline-primary kh" onClick={(evt) => changeLanguage('kh')}>ខ្មែរ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CustomerDashboard;