import React from 'react';
import ReactDOM from 'react-dom';
import './loading.style.css';

export function Loading() {
    return ReactDOM.createPortal(
        <div id="systemLoading" className="loading d-none">
            <div className="spinner-border text-danger p-5" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>,
        document.getElementById('loadingContainer')
    );
}

