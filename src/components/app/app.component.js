import React, { useEffect } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import CustomerDashboard from '../customer-dashboard/customer-dashboard.component';
import Login from '../login/login.component';
import Modal from '../modal/modal.component';
import AdminDashboard from '../admin-dashboard/admin-dashboard.component';
import { Loading } from '../loading/loading.component';
import { useLoadUserData } from '../../hooks/load-user.hook';
import { useDispatch } from 'react-redux';
import { AuthActionType } from '../../redux/reducers/auth.reducer';
import { AuthRoute } from '../auth-route/auth-route.component';

function App() {

    const dispatch = useDispatch();
    const userData = useLoadUserData();

    if (userData !== null) {
        dispatch({
            type: AuthActionType.LOAD,
            payload: userData
        });
    }

    return (
        <BrowserRouter>
            <div className="app w-100 h-100 disable-selection">

                <Switch>
                    <AuthRoute path="/customer" component={CustomerDashboard} />
                    <AuthRoute path="/admin" component={AdminDashboard} />
                    <Route path={['/', '/login']} component={Login} />
                </Switch>
            </div>
            <Modal />
            <Loading />
        </BrowserRouter>
    );
}

export default App;