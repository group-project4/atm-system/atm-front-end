export const loginSchema = Object.freeze({
    username: {
        presence: {
            allowEmpty: false
        }
    },
    password: {
        presence: {
            allowEmpty: false
        }
    }
});