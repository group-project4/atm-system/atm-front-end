import React, { useEffect, useState } from 'react';
import './login.style.css';
import { useHistory, Redirect } from 'react-router-dom';
import { useFormValidation } from '../../hooks/form-validation.hook';
import { loginSchema } from './login-schema.validation';
import { useAPI } from '../../hooks/api.hook';
import Axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { AuthActionType } from './../../redux/reducers/auth.reducer';
import { useAxiosErrorHandler } from '../../hooks/axios-error-handler.hook';
import { showLoading, hideLoading } from './../../assets/js/loading';
import { useCheckAuth } from '../../hooks/auth.hook';
import { AlertMessage, AlertType } from '../../assets/js/alert';

function Login() {
    const history = useHistory();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);
    const [form, validateForm] = useFormValidation(loginSchema);
    const ApiEndPoint = useAPI();
    const dispatch = useDispatch();
    const axiosErrorHandler = useAxiosErrorHandler();
    const isLoggedIn = useCheckAuth();
    const { role } = useSelector((state) => state.authReducer);

    function onFormSubmit(evt) {
        evt.preventDefault();
        if (!validateForm()) {
            setAutoValidate(true);
            return;
        }
        
        showLoading();
        Axios.post(ApiEndPoint.LOGIN, {
            username,
            password
        })
        .then((response) => {
            return response.data;
        })
        .then((jsonResponse) => {
            const token = jsonResponse.data.token;
            const role = jsonResponse.data.role;
            const username = jsonResponse.data.username;
            const isActive = jsonResponse.data.is_active;

            if (!isActive) {
                return new AlertMessage({
                    message: 'Your account has been disabled.',
                    alertType: AlertType.WARNING
                }).show();
            }

            // success, then save account information
            dispatch({
                type: AuthActionType.SAVE,
                payload: {
                    token,
                    role,
                    username
                }
            });

            if (role === 'admin') {
                history.push('/admin')
            } else {
                history.push('/customer')
            }
        })
        .catch((error) => {
            axiosErrorHandler(error);
        })
        .finally(() => {
            hideLoading();
        });
        
    }

    useEffect(() => {
        if (autoValidate)
            validateForm();
    }, [username, password, autoValidate]);

    if (isLoggedIn) {
        if (role === 'admin') {
            return (
                <Redirect to="/admin" />
            )
        } else if (role === 'normal') {
            return (
                <Redirect to="/customer" />
            )
        }
        
    }

    return (
        <div className="w-100 h-100" style={{backgroundColor: '#0a3d62', minHeight: '100vh'}}>
            <form ref={form} className="login-form shadow-lg" onSubmit={onFormSubmit}>
                <h2 className="text-center font-weight-bold">ATM</h2>
                <div className="form-group">
                    <label htmlFor="username">Username</label>
                    <input id="username" type="text" className="form-control" onChange={(evt) => setUsername(evt.currentTarget.value)} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input id="password" type="password" className="form-control" onChange={(evt) => setPassword(evt.currentTarget.value)} />
                </div>
                <div className="form-group">
                    <button className="btn btn-danger shadow-sm float-right" type="submit">Login</button>
                </div>
            </form>
        </div>
    )
}

export default Login;