import React, { useState } from 'react';
import {
    useAPI,
    useAPIHeader,
    useAxiosErrorHandler,
    useCentToDollarConverter
} from './../../../hooks';
import { useEffect } from 'react';
import Axios from 'axios';
import { hideLoading, showLoading } from '../../../assets/js/loading';
import moment from 'moment';

function FormWithdrawal() {
    
    const [withdrawalTransactions, setWithdrawalTransactions] = useState([]);

    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();
    const centToDollar = useCentToDollarConverter();

    function getDepositTransaction() {
        showLoading();
        Axios.get(APIEndPoint.GET_WITHDRAWAL_TRANSACTION, {
            headers: apiHeader
        })
        .then((response) => response.data)
        .then((jsonResponse) => setWithdrawalTransactions(jsonResponse.data))
        .catch((error) => handleAxiosError(error))
        .finally(() => hideLoading());
        
    }

    useEffect(() => {
        getDepositTransaction();
    }, []);

    return (
        <div id="formService">
            <div className="d-flex">
                <form className="flex-grow-1 shadow">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">
                                <i className="fas fa-search"></i>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Search" />
                    </div>
                </form>
                <div className="btn-group ml-3"></div>
            </div>
            <div className="table-responsive mt-3 shadow">
                <table className="table table-hover table-striped">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Customer Account</th>
                            <th scope="col">Withdrew Amount (USD)</th>
                            <th scope="col">Transaction Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {withdrawalTransactions.map((withdrawalTransaction) => {
                            return (
                                <tr key={withdrawalTransaction.deposit_id}>
                                    <td>{withdrawalTransaction.account.account_name}</td>
                                    <td>{centToDollar(withdrawalTransaction.withdraw_amount)}</td>
                                    <td>{moment(withdrawalTransaction.withdraw_date).format('DD-MM-YYYY hh:mm A')}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default FormWithdrawal;