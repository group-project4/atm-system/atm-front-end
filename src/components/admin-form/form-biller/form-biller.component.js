import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { ModalActionType } from '../../../redux/reducers/modal.reducer';
import FormCreateServiceType from '../form-create-service-type/form-create-service-type.component';
import FormCreateBiller from '../form-create-biller/form-create-biller.component';
import Axios from 'axios';
import { AlertMessage, AlertType } from './../../../assets/js/alert';
import { showLoading, hideLoading } from './../../../assets/js/loading';
import { useAPIHeader } from './../../../hooks/api-header.hook';
import { useAPI } from './../../../hooks/api.hook';
import { useAxiosErrorHandler } from './../../../hooks/axios-error-handler.hook';

function FormBiller(props) {
    // state
    const [billers, setBillers] = useState([]);

    const dispatch = useDispatch();
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();

    function getAllBillers() {
        showLoading();
        Axios.get(APIEndPoint.GET_BILLERS, {
            headers: apiHeader
        })
        .then((response) => {
            return response.data;
        })
        .then((jsonResponse) => {
            setBillers(jsonResponse.data);
        })
        .catch((error) => {
            handleAxiosError(error);
        })
        .finally(() => {
            hideLoading();
        });
    }

    useEffect(() => {
        getAllBillers();
    }, []);

    return (
        <div id="formBiller">
            <div className="d-flex">
                <form className="flex-grow-1 shadow">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">
                                <i className="fas fa-search"></i>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Search" />
                    </div>
                </form>
                <div className="btn-group ml-3 shadow">
                    <button type="button" className="btn btn-primary"
                        onClick={(evt) => dispatch({
                            type: ModalActionType.OPEN_MODAL,
                            payload: {
                                title: 'Register Biller',
                                form: <FormCreateBiller />
                            }
                        })}
                    >Create Biller</button>
                    <button type="button" className="btn btn-primary"
                        onClick={(evt) => dispatch({
                            type: ModalActionType.OPEN_MODAL,
                            payload: {
                                title: 'Create Service Type',
                                form: <FormCreateServiceType />
                            }
                        })}
                    >Create Service</button>
                </div>
            </div>
            <div className="table-responsive mt-3 shadow">
                <table className="table table-hover table-striped">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Biller Name</th>
                            <th scope="col">Service Type</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Logo</th>
                            <th scope="col">Email</th>
                            <th scope=""></th>
                        </tr>
                    </thead>
                    <tbody>
                        {billers.map((biller) => {
                            return (
                                <tr key={biller.biller_id}>
                                    <td>{biller.biller_name}</td>
                                    <td>{biller.biller_type.biller_type_name}</td>
                                    <td>{biller.biller_phone}</td>
                                    <td>{biller.biller_logo}</td>
                                    <td>{biller.biller_email}</td>
                                    <td></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default FormBiller;