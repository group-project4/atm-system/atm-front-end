export const createBillerSchema = Object.freeze({
    billerName: {
        presence: {
            allowEmpty: false
        },
        length: {
            maximum: 100
        }
    },
    billerLocation: {
        presence: {
            allowEmpty: false
        }
    },
    billerPhone: {
        presence: {
            allowEmpty: false
        },
        length: {
            maximum: 30
        }
    },
    email: {
        presence: {
            allowEmpty: false
        },
        email: true,
        length: {
            maximum: 255
        }
    },
    billerType: {
        presence: {
            allowEmpty: false
        }
    }
});