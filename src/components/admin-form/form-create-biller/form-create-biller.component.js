import React, { useState, useEffect } from 'react';
import { createBillerSchema } from './validate-schema';
import { useAxiosErrorHandler } from './../../../hooks/axios-error-handler.hook';
import { useFormValidation } from './../../../hooks/form-validation.hook';
import { useAPI } from './../../../hooks/api.hook';
import { useAPIHeader } from './../../../hooks/api-header.hook';
import { hideLoading, showLoading } from './../../../assets/js/loading';
import { AlertMessage, AlertType } from './../../../assets/js/alert';
import Axios from 'axios';

function FormCreateBiller(props) {
    const [billerName, setBillerName] = useState('');
    const [billerLocation, setBillerLocation] = useState('');
    const [billerPhone, setBillerPhone] = useState('');
    const [email, setEmail] = useState('');
    const [remark, setRemark] = useState('');
    const [logo, setLogo] = useState('');
    const [billerType, setBillerType] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);

    // form data
    const [billerTypes, setBillerTypes] = useState([]);

    const [imageName, setImageName] = useState('Choose File');
    const [imageUrl, setImageUrl] = useState('');
    const [file, setFile] = useState(null);

    const [form, validateForm] = useFormValidation(createBillerSchema);
    const handleAxiosError = useAxiosErrorHandler();
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();

    async function onFormSubmit(evt) {
        evt.preventDefault();

        if (!validateForm())
            return setAutoValidate(true);

        showLoading();

        // upload file
        const formData = new FormData();
        formData.append('file', file);

        let uploadedFileName = 'no-image';

        try {
            const response = await Axios.post(APIEndPoint.UPLOAD_FILE, formData, {
                headers: apiHeader
            });
            uploadedFileName = response.data.data.file_name;
        } catch (error) {
            handleAxiosError(error);
        }

        const requestBody = {
            biller_name: billerName,
            biller_location: billerLocation,
            biller_phone: billerPhone,
            biller_email: email,
            biller_remark: remark,
            biller_logo: uploadedFileName,
            biller_type_id: Number(billerType)
        };
        
        if (!remark)
            delete requestBody['biller_remark'];

        try {
            await Axios.post(APIEndPoint.CREATE_BILLER, requestBody, {
                headers: apiHeader
            });
            new AlertMessage({
                message: 'New biller has been created',
                alertType: AlertType.SUCCESS
            }).show();
            resetForm();
        } catch (error) {
            console.log(error.response.data);
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    function resetForm() {
        setBillerName('');
        setBillerLocation('');
        setBillerPhone('');
        setEmail('');
        setRemark('');
        setLogo('');
        setBillerType('');
        setAutoValidate(false);
        setImageName('');
        setImageUrl('');
        setFile(null);
    }

    function getBillerTypes() {
        showLoading();
        Axios.get(APIEndPoint.GET_BILLER_TYPES, {
            headers: apiHeader
        })
        .then((response) => {
            return response.data;
        })
        .then((jsonResponse) => {
            setBillerTypes(jsonResponse.data);
        })
        .catch((error) => {
            handleAxiosError(error);
        })
        .finally(() => {
            hideLoading();
        });
    }

    useEffect(() => {
        if (autoValidate)
            validateForm();
    }, [billerName, billerLocation, billerPhone, email, logo, billerType, autoValidate]);

    useEffect(() => {
        getBillerTypes();
    }, []);

    return (
        <form ref={form} onSubmit={onFormSubmit} noValidate>
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Biller Name</label>
                        <input id="billerName" type="text" className="form-control" value={billerName} onChange={(evt) => setBillerName(evt.currentTarget.value)} />
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Phone</label>
                        <input id="billerPhone" type="text" className="form-control" value={billerPhone} onChange={(evt) => setBillerPhone(evt.currentTarget.value)} />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Email</label>
                        <input id="email" type="text" className="form-control" value={email} onChange={(evt) => setEmail(evt.currentTarget.value)} />
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Service Type</label>
                        <select id="billerType" className="custom-select" value={billerType} onChange={(evt) => setBillerType(evt.currentTarget.value)} >
                            <option value="">-- Select --</option>
                            {billerTypes.map((billerType) => <option key={billerType.biller_type_id} value={billerType.biller_type_id}>{billerType.biller_type_name}</option>)}
                        </select>
                    </div>
                </div>
            </div>
            <div className="form-group">
                <label>Address</label>
                <textarea id="billerLocation" type="text" className="form-control" value={billerLocation} onChange={(evt) => setBillerLocation(evt.currentTarget.value)}></textarea>
            </div> 
            <div className="form-group">
                <label>Remark</label>
                <textarea id="remark" type="text" className="form-control" value={remark} onChange={(evt) => setRemark(evt.currentTarget.value)} />
            </div>
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label htmlFor="logo">Logo</label>
                        <div className="custom-file">
                            <input id="file" type="file" className="custom-file-input" accept="image/*"
                                onChange={(evt) => {
                                    const files = evt.currentTarget.files;
                                    if (files.length > 0) {
                                        setImageUrl(URL.createObjectURL(files[0]));
                                        setImageName(files[0].name)
                                        setFile(files[0])
                                    } else {
                                        setImageUrl('');
                                        setImageName('Choose File');
                                        setFile(null);
                                    }
                                }}
                            />
                            <label className="custom-file-label">{imageName}</label>
                        </div>
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="d-flex align-item-center justify-content-center">
                        <img style={{width: '180px', height: 'auto'}} src={imageUrl} alt="" />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right">Done</button>
            </div>
        </form>
    );
}

export default FormCreateBiller;