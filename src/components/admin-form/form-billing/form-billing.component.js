import React, { useState } from 'react';
import {
    useAPI,
    useAPIHeader,
    useAxiosErrorHandler,
    useCentToDollarConverter
} from './../../../hooks';
import { useEffect } from 'react';
import Axios from 'axios';
import { hideLoading, showLoading } from '../../../assets/js/loading';
import moment from 'moment';

function FormBilling() {
    
    const [billings, setBillings] = useState([]);

    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();
    const centToDollar = useCentToDollarConverter();

    function getBillingTransaction() {
        showLoading();
        Axios.get(APIEndPoint.GET_BILLING_TRANSACTION, {
            headers: apiHeader
        })
        .then((response) => response.data)
        .then((jsonResponse) => setBillings(jsonResponse.data))
        .catch((error) => handleAxiosError(error))
        .finally(() => hideLoading());
        
    }

    useEffect(() => {
        getBillingTransaction();
    }, []);

    return (
        <div id="formService">
            <div className="d-flex">
                <form className="flex-grow-1 shadow">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">
                                <i className="fas fa-search"></i>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Search" />
                    </div>
                </form>
                <div className="btn-group ml-3"></div>
            </div>
            <div className="table-responsive mt-3 shadow">
                <table className="table table-hover table-striped">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Customer Account</th>
                            <th scope="col">Amount (USD)</th>
                            <th scope="col">Biller Name</th>
                            <th scope="col">Biller Type</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {billings.map((billing) => {
                            return (
                                <tr key={billing.billing_id}>
                                    <td>{billing.customer.account_name}</td>
                                    <td>{centToDollar(billing.billing_amount)}</td>
                                    <td>{billing.biller.biller_name}</td>
                                    <td>{billing.biller.biller_type.biller_type_name}</td>
                                    <td>{moment(billing.billing_date).format('DD-MM-YYYY hh:mm A')}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default FormBilling;