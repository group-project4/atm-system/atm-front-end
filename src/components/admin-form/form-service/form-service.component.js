import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { useAxiosErrorHandler } from '../../../hooks/axios-error-handler.hook';
import { useAPI } from '../../../hooks/api.hook';
import { useAPIHeader } from '../../../hooks/api-header.hook';
import { hideLoading, showLoading } from './../../../assets/js/loading';
import FormCreateServiceType from './../../admin-form/form-create-service-type/form-create-service-type.component';
import { ModalActionType } from './../../../redux/reducers/modal.reducer';
import { useDispatch } from 'react-redux';

function FormService() {
    // state
    const [serviceTypes, setServiceTypes] = useState([]);
;
    const handleAxiosError = useAxiosErrorHandler();
    const apiHeader = useAPIHeader();
    const APIEndPoint = useAPI();
    const dispatch = useDispatch();

    function getServiceTypes() {
        showLoading();
        Axios.get(APIEndPoint.GET_BILLER_TYPES, {
            headers: apiHeader
        })
        .then((response) => {
            return response.data;
        })
        .then((jsonResponse) => {
            setServiceTypes(jsonResponse.data);
        })
        .catch((error) => {
            handleAxiosError(error);
        })
        .finally(() => {
            hideLoading();
        });
    }

    useEffect(() => {
        //showLoading();
        getServiceTypes();
    }, []);

    return (
        <div id="formService">
            <div className="d-flex">
                <form className="flex-grow-1 shadow">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">
                                <i className="fas fa-search"></i>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Search" />
                    </div>
                </form>
                <div className="btn-group ml-3">
                    <button type="button" className="btn btn-primary"
                        onClick={(evt) => dispatch({
                            type: ModalActionType.OPEN_MODAL,
                            payload: {
                                title: 'Create Service Type',
                                form: <FormCreateServiceType />
                            }
                        })}
                    >Create Service</button>
                </div>
            </div>
            <div className="table-responsive mt-3 shadow">
                <table className="table table-hover table-striped">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Service Type</th>
                            <th scope="col">Total Biller</th>
                        </tr>
                    </thead>
                    <tbody>
                        {serviceTypes.map((service) => {
                            return (
                                <tr key={service.biller_type_id}>
                                    <td>{service.biller_type_name}</td>
                                    <td>{service.total_biller}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default FormService;