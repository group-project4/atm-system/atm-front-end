export const createServiceTypeSchema = Object.freeze({
    serviceType: {
        presence: {
            allowEmpty: false
        },
        length: {
            maximum: 100
        }
    }
});