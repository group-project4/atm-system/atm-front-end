import React, { useState, useEffect } from 'react';
import { createServiceTypeSchema } from './service-type-schema';
import { useFormValidation } from './../../../hooks/form-validation.hook';
import Axios from 'axios';
import { useAPIHeader } from '../../../hooks/api-header.hook';
import {  useAPI} from './../../../hooks/api.hook';
import { useAxiosErrorHandler } from './../../../hooks/axios-error-handler.hook';
import { hideLoading, showLoading } from '../../../assets/js/loading';
import { AlertMessage, AlertType } from '../../../assets/js/alert';

function FormCreateServiceType() {
    const [serviceType, setServiceType] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);

    const [form, validateForm] = useFormValidation(createServiceTypeSchema);
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();

    async function onFormSubmit(evt) {
        evt.preventDefault();

        if (!validateForm()) {
            return setAutoValidate(true);
        }

        const requestBody = {
            biller_type_name: serviceType
        };

        showLoading();

        try {
            await Axios.post(APIEndPoint.CREATE_BILLER_TYPE, requestBody, {
                headers: apiHeader
            });
            new AlertMessage({
                message: 'Biller service type has created successfully.',
                alertType: AlertType.SUCCESS
            }).show();
            resetForm();
        } catch (error) {
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    function resetForm() {
        setAutoValidate(false);
        setServiceType('');
    }

    useEffect(() => {
        if (autoValidate)
            validateForm();
    }, [serviceType, autoValidate])

    return (
        <form ref={form} onSubmit={onFormSubmit} noValidate>
            <div className="form-group">
                <label>Service Type</label>
                <input id="serviceType" type="text" className="form-control" value={serviceType} onChange={(evt) => setServiceType(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right">Done</button>
            </div>
        </form>
    );
}

export default FormCreateServiceType;