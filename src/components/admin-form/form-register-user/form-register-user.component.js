import React, { useRef, useState, useEffect } from 'react';
import { useFormValidation } from './../../../hooks/form-validation.hook';
import { useAPIHeader } from './../../../hooks/api-header.hook';
import { constraints } from './validate-schema';
import { useAxiosErrorHandler } from './../../../hooks/axios-error-handler.hook';
import Axios from 'axios';
import { useAPI } from '../../../hooks/api.hook';
import { showLoading, hideLoading } from '../../../assets/js/loading';
import { AlertMessage, AlertType } from './../../../assets/js/alert';

function FormRegisterUser() {

    // states
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [gender, setGender] = useState('');
    const [email, setEmail] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [address, setAddress] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [role, setRole] = useState('');
    const [imageUrl, setImageUrl] = useState();
    const [imageName, setImageName] = useState();
    const [dob, setDoB] = useState('');
    const [file, setFile] = useState(null);
    const [autoFormValidate, setAutoFormValidate] = useState(false);
    const [formRef, validateForm] = useFormValidation(constraints);
    const ApiEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const axiosErrorHandler = useAxiosErrorHandler();

    async function onFormSubmit(evt) {
        evt.preventDefault();
        if (!validateForm()) {
            return setAutoFormValidate(true);
        }

        // upload image
        const formData = new FormData();
        formData.append('file', file);

        let uploadedFileName = 'no-file';

        showLoading();

        try {
            const response = await Axios.post(ApiEndPoint.UPLOAD_FILE, formData, {
                headers: apiHeader
            });
            uploadedFileName = response.data.data.file_name;
        } catch (error) {
            axiosErrorHandler(error);
        }
        
        const requestBody = {
            user_firstname: firstName,
            user_lastname: lastName,
            user_gender: gender,
            user_email: email,
            user_phone: phoneNumber,
            user_address: address,
            user_username: username,
            user_password: password,
            user_role: role,
            user_dob: dob,
            user_photo: uploadedFileName
        };

        try {
            await Axios.post(ApiEndPoint.CREATE_USER, requestBody, {
                headers: apiHeader
            });   
            new AlertMessage({
                message: 'New user has been created.',
                alertType: AlertType.SUCCESS
            }).show();
            resetForm();
        } catch (error) {
            axiosErrorHandler(error);
        } finally {
            hideLoading();
        }
    }

    function resetForm() {
        setAutoFormValidate(false);
        setFirstName('');
        setLastName('');
        setGender('');
        setEmail('');
        setPhoneNumber('');
        setAddress('');
        setUsername('');
        setPassword('');
        setRole('');
        setDoB('');
        setImageUrl('');
        setImageName('');
        setConfirmPassword('');
    }

    useEffect(() => {
        if (autoFormValidate)
            validateForm();
    }, [firstName, lastName, gender, email, phoneNumber, address, username, password, confirmPassword, role, imageUrl, dob, autoFormValidate]);

    return (
        <form id="formRegisterUser" onSubmit={onFormSubmit} ref={formRef} noValidate>
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>First Name</label>
                        <input id="firstName" type="text" className="form-control"
                            value={firstName}
                            onChange={(evt) => setFirstName(evt.currentTarget.value)}
                        />
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Last Name</label>
                        <input id="lastName" type="text" className="form-control"
                            value={lastName}
                            onChange={(evt) => setLastName(evt.currentTarget.value)}
                        />
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Gender</label>
                        <select id="gender" className="custom-select"
                            value={gender}
                            onChange={(evt) => setGender(evt.currentTarget.value)}
                        >
                            <option value="">-- Select --</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Phone</label>
                        <input id="phoneNumber" type="text" className="form-control"
                            value={phoneNumber}
                            onChange={(evt) => setPhoneNumber(evt.currentTarget.value)}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <label>Email</label>
                <input id="email" type="email" className="form-control"
                    value={email}
                    onChange={(evt) => setEmail(evt.currentTarget.value)}
                />
            </div>
            <div className="form-group">
                <label>Address</label>
                <textarea id="address" type="text" className="form-control" style={{minHeight: '90px'}}
                    value={address}
                    onChange={(evt) => setAddress(evt.currentTarget.value)}
                />
            </div>
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label htmlFor="username">User Name</label>
                        <input id="username" type="text" className="form-control"
                            value={username}
                            onChange={(evt) => setUsername(evt.currentTarget.value)}
                        />
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Password</label>
                        <input id="password" type="password" className="form-control"
                            value={password}
                            onChange={(evt) => setPassword(evt.currentTarget.value)}
                        />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <label>Confirm Password</label>
                <input id="confirmPassword" type="password" className="form-control"
                    value={confirmPassword}
                    onChange={(evt) => setConfirmPassword(evt.currentTarget.value)}
                />
            </div>
            <div className="row">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label>Photo</label>
                        <div className="custom-file">
                            <input id="file" type="file" className="custom-file-input" accept="image/*"
                                onChange={(evt) => {
                                    const files = evt.currentTarget.files;
                                    if (files.length > 0) {
                                        setImageUrl(URL.createObjectURL(files[0]));
                                        setImageName(files[0].name);
                                        setFile(files[0]);
                                    } else {
                                        setImageUrl('');
                                        setImageName('');
                                        setFile(null);
                                    }
                                }}
                            />
                            <label className="custom-file-label">{imageName}</label>
                        </div>
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label htmlFor="role">Role</label>
                        <select id="role" className="custom-select" 
                            value={role}
                            onChange={(evt) => setRole(evt.currentTarget.value)}
                        >
                            <option value="">-- Select --</option>
                            <option value="admin">Admin</option>
                            <option value="normal">Normal</option>
                        </select>
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="d-flex justify-content-center">
                        <img className="border" style={{width: '180px', height: 'auto'}} src={imageUrl} alt="" />
                    </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div className="form-group">
                        <label htmlFor="dob">Date of Birth</label>
                        <input id="dob" type="date" className="form-control" value={dob} onChange={(evt) => setDoB(evt.currentTarget.value)} />
                    </div>
                </div>
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right">Done</button>
            </div> 
        </form>
    );
}

export default FormRegisterUser;