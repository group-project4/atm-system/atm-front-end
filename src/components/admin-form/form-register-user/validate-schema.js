// validation schema
export const constraints = Object.freeze({
    firstName: {
        presence: {
            allowEmpty: false
        },
        length: {
            maximum: 100
        }
    },
    lastName: {
        presence: {
            allowEmpty: false
        },
        length: {
            maximum: 100
        }
    },
    gender: {
        presence: {
            allowEmpty: false
        },
        inclusion: {
            within: {
                'male': 'male',
                'female': 'female'
            }
        }
    },
    phoneNumber: {
        presence: {
            allowEmpty: false
        },
        length: {
            minimum: 8,
            maximum: 30
        }
    },
    email: {
        presence: {
            allowEmpty: false
        },
        email: true,
        length: {
            maximum: 100
        }
    },
    address: {
        presence: {
            allowEmpty: false
        },
        length: {
            maximum: 255
        }
    },
    username: {
        presence: {
            allowEmpty: false
        },
        length: {
            maximum: 100
        }
    },
    password: {
        presence: {
            allowEmpty: false
        },
        length: {
            minimum: 5
        }
    },
    confirmPassword: {
        presence: {
            allowEmpty: false
        },
        equality: 'password'
    },
    role: {
        presence: {
            allowEmpty: false
        },
        inclusion: {
            within: {
                'admin': 'Admin',
                'normal': 'Normal'
            }
        }
    },
    file: {
        presence: {
            allowEmpty: false
        }
    },
    dob: {
        presence: {
            allowEmpty: false
        }
    }
});