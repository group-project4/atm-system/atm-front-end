import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ModalActionType } from '../../../redux/reducers/modal.reducer';
import FormRegisterUser from '../form-register-user/form-register-user.component';
import Axios from 'axios';
import { useAPI } from '../../../hooks/api.hook';
import { useAPIHeader } from '../../../hooks/api-header.hook';
import { useAxiosErrorHandler } from '../../../hooks/axios-error-handler.hook';
import { showLoading, hideLoading } from './../../../assets/js/loading';
import FormChangePassword from './../../form/change-password/change-password.component';
import FormDisableAccount from './../../admin-form/form-disable-account/form-disable-account.component';
import moment from 'moment';

function FormUser(props) {
    // state
    const [users, setUsers] = useState([]);

    const dispatch = useDispatch();
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();
    const currentUser = useSelector((state) => state.authReducer);

    function getUsers() {
        showLoading();
        Axios.get(APIEndPoint.GET_USERS, {
            headers: apiHeader
        })
        .then((response) => {
            return response.data;
        })
        .then((jsonResponse) => {
            setUsers(jsonResponse.data);
        })
        .catch((error) => {
            handleAxiosError(error);
        })
        .finally(() => {
           hideLoading();
        });
    }

    useEffect(() => {
        getUsers();
    }, []);

    return (
        <div id="formUser">
            <div className="d-flex">
                <form className="d-inline-flex flex-grow-1 shadow">
                    <div className="input-group">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="basic-addon1">
                                <i className="fas fa-search"></i>
                            </span>
                        </div>
                        <input type="text" className="form-control" placeholder="Search" />
                    </div>
                </form>
                <div className="btn-group ml-3 shadow">
                    <button type="button" className="btn btn-primary"
                        onClick={(evt) => dispatch({
                            type: ModalActionType.OPEN_MODAL,
                            payload: {
                                title: 'Register User',
                                form: <FormRegisterUser />
                            }
                        })}
                    >Register</button>
                    <button type="button" className="btn btn-warning"
                        onClick={(evt) => 
                            dispatch({
                                type: ModalActionType.OPEN_MODAL,
                                payload: {
                                    title: 'Change Password',
                                    form: <FormChangePassword />
                                }
                            })
                        }
                    >Change Password</button>
                </div>
            </div>
            <div className="table-responsive shadow mt-3">
                <table className="table table-hover table-striped">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Gender</th>
                            <th scope="col">DOB</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Email</th>
                            <th scope="col">Address</th>
                            <th scope=""></th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user) => {
                            return (
                                <tr key={user.user_id} className={user.is_active ? '' : 'text-danger'}>
                                    <td>{user.user_firstname}</td>
                                    <td>{user.user_lastname}</td>
                                    <td>{user.user_gender}</td>
                                    <td>{moment(user.user_dob).format('DD-MM-YYYY')}</td>
                                    <td>{user.user_phone}</td>
                                    <td>{user.user_email}</td>
                                    <td>{user.user_address}</td>
                                    <td className="text-right">
                                        <div className="btn-group">
                                            <button type="button" className="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                            <div className="dropdown-menu dropdown-menu-right">
                                                {(() => {
                                                    // do not show diable option for own user.
                                                    if (user.user_username !== currentUser.username) {
                                                        if (user.is_active) {
                                                            return (
                                                                <button className="dropdown-item text-danger" 
                                                                    onClick={(evt) => {
                                                                        dispatch({
                                                                            type: ModalActionType.OPEN_MODAL,
                                                                            payload: {
                                                                                form: <FormDisableAccount userId={user.user_id} isActive={false} />,
                                                                                title: 'Disable account confirmation'
                                                                            }
                                                                        })
                                                                    }}
                                                                >Disable</button>
                                                            )
                                                        } else {
                                                            return (
                                                                <button className="dropdown-item text-success" 
                                                                    onClick={(evt) => {
                                                                        dispatch({
                                                                            type: ModalActionType.OPEN_MODAL,
                                                                            payload: {
                                                                                form: <FormDisableAccount userId={user.user_id} isActive={true} />,
                                                                                title: 'Enable account confirmation'
                                                                            }
                                                                        })
                                                                    }}
                                                                >Enable</button>
                                                            )
                                                        }
                                                    }
                                                })()}
                                                <button className="dropdown-item"
                                                >View Account Details</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            ) 
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default FormUser;