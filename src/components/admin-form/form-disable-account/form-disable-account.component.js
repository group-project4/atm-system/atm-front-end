import React from 'react';
import Axios from 'axios';
import { useDispatch } from 'react-redux';
import { ModalActionType } from '../../../redux/reducers/modal.reducer';
import { useAPI } from '../../../hooks/api.hook';
import { useAPIHeader } from '../../../hooks/api-header.hook';
import { useAxiosErrorHandler } from '../../../hooks/axios-error-handler.hook';
import { showLoading, hideLoading } from './../../../assets/js/loading';
import { AlertMessage, AlertType } from '../../../assets/js/alert';

function FormDisableAccount({ userId, isActive }) {

    const dispatch = useDispatch();
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();

    async function disableAccount() {
        const requestBody = {
            user_id: userId,
            is_active: isActive,
        };

        showLoading();
        Axios.put(APIEndPoint.UPDATE_USER_STATUS, requestBody, {
            headers: apiHeader
        })
        .then((response) => {
            new AlertMessage({
                message: isActive ? 'Account enabled' : 'Account disabled',
                alertType: AlertType.SUCCESS
            }).show();
        })
        .catch((error) => handleAxiosError(error))
        .finally(() => {
            hideLoading()
            dispatch({
                type: ModalActionType.CLOSE_MODAL
            });
        });
    }
    
    function onFormSubmit(evt) {
        evt.preventDefault();
        disableAccount();
    }

    return (
        <form onSubmit={onFormSubmit}>
            <h5 className="text-danger text-center">{`Are you sure to ${isActive ? 'enable' : 'disable'} this account ?`}</h5>
            <div className="d-flex justify-content-center">
                <div className="btn-group mt-3">
                    <button type="submit" className="btn btn-primary shadow">Confirm</button>
                    <button type="button" className="btn btn-warning shadow" 
                        onClick={(evt) => dispatch({ type: ModalActionType.CLOSE_MODAL })}
                    >Cancel</button>
                </div>
            </div>
        </form>
    )
}

export default FormDisableAccount;