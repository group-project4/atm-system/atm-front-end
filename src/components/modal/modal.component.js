import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import $ from 'jquery';
import { ModalActionType } from '../../redux/reducers/modal.reducer';

function Modal() {
    const dispatch = useDispatch();
    const {form, title} = useSelector((state) => state.modalReducer);
    
    useEffect(() => {
        if (!form)
            return;
        $('#modalForm').on('hidden.bs.modal', (evt) => {
            dispatch({
                type: ModalActionType.CLOSE_MODAL,
                payload: {}
            });
        });
        $('#modalForm').modal('show');
        
    }, [form, title]);

    useEffect(() => {
        if (form) return;

        $('#modalForm').modal('hide');
        
    }, [form]);

    return (
        <div id="modalForm" className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header" style={{backgroundColor: '#0a3d62'}}>
                        <h5 className="modal-title text-light font-weight-bold" id="exampleModalLabel">{title}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" className="text-light">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        {form}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Modal;