export const createDepositSchema = Object.freeze({
    depositAmount: {
        presence: {
            allowEmpty: false
        },
        numericality: true,
        currency: true
    }
});