import React, { useState, useEffect } from 'react';
import { hideLoading, showLoading } from '../../../assets/js/loading';
import { AlertMessage, AlertType } from '../../../assets/js/alert';
import { useAxiosErrorHandler } from '../../../hooks/axios-error-handler.hook';
import { useAPI } from '../../../hooks/api.hook';
import { useFormValidation } from '../../../hooks/form-validation.hook';
import { useAPIHeader } from '../../../hooks/api-header.hook';
import Axios from 'axios';
import { createDepositSchema } from './deposit-schema';
import { useDispatch } from 'react-redux';
import { ModalActionType } from './../../../redux/reducers/modal.reducer';
import { useDollarToCentConverter } from './../../../hooks/balance-converter.hook';
import { TransactionActionType } from '../../../redux/reducers/transaction.reducer';

function FormDeposit() {
    const [depositAmount, setDepositAmount] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);
    const dispatch = useDispatch(); 

    const handleAxiosError = useAxiosErrorHandler();
    const [form, validateForm] = useFormValidation(createDepositSchema);
    const apiHeader = useAPIHeader();
    const APIEndPoint = useAPI();
    const convertBalance = useDollarToCentConverter();

    async function onFormSubmit(evt) {
        evt.preventDefault();

        if (!validateForm())
            return setAutoValidate(true);

        const requestBody = {
            deposit_amount: convertBalance(depositAmount)
        };

        showLoading();
        try {
            await Axios.post(APIEndPoint.DEPOSIT, requestBody, {
                headers: apiHeader
            });
            new AlertMessage({
                message: 'Your deposit transaction has been successfully processed',
                alertType: AlertType.SUCCESS
            }).show();

            //
            dispatch({
                type: TransactionActionType.HAS_TRANSACTION,
                payload: {
                    transactionBalance: convertBalance(depositAmount)
                }
            });

            // close modal
            dispatch({
                type: ModalActionType.CLOSE_MODAL
            });
        } catch (error) {
            handleAxiosError(error);   
        } finally {
            hideLoading();
        }
    }

    useEffect(() => {
        if (!autoValidate) return;

        validateForm();

    }, [depositAmount, autoValidate])

    return (
        <form ref={form} onSubmit={onFormSubmit} noValidate>
            <div className="form-group">
                <label>Deposit Amount</label>
                <input id="depositAmount" type="text" className="form-control" value={depositAmount} onChange={(evt) => setDepositAmount(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right">Done</button>
            </div>
        </form>
    );
}

export default FormDeposit;