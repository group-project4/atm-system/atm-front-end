import React from 'react';
import { ModalActionType } from './../../../redux/reducers/modal.reducer';
import { useDispatch } from 'react-redux';

function AccountInformation({
    accountName,
    accountNumber,
    balance,
    phoneNumber,
    registrationDate,

}) {
    const dispatch = useDispatch();

    function formSubmit(evt) {
        evt.preventDefault();
        dispatch({
            type: ModalActionType.CLOSE_MODAL
        });
    }
   
    return (
        <form onSubmit={formSubmit}>
            <div className="form-group">
                <label>Account Name</label>
                <input type="text" value={accountName} className="form-control" disabled />
            </div>
            <div className="form-group">
                <label>Account Number</label>
                <input type="text" value={accountNumber} className="form-control" disabled />
            </div>
            <div className="form-group">
                <label>Balance (USD)</label>
                <input type="text" value={balance} className="form-control" disabled />
            </div>
            <div className="form-group">
                <label>Phone Number</label>
                <input type="text" value={phoneNumber} className="form-control" disabled />
            </div>
            <div className="form-group">
                <label>Registration Date</label>
                <input type="text" value={registrationDate} className="form-control" disabled />
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right">Close</button>
            </div>
        </form>
    );
}

export default AccountInformation;