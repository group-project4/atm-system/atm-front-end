export const changePasswordSchema = Object.freeze({
    newPassword: {
        presence: {
            allowEmpty: false
        },
        length: {
            minimum: 5
        }
    },
    confirmPassword: {
        presence: {
            allowEmpty: false
        },
        length: {
            minimum: 5
        },
        equality: 'newPassword'
    },
    oldPassword: {
        presence: {
            allowEmpty: false
        },
        length: {
            minimum: 5
        }
    }
});