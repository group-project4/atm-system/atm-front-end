import React, { useState, useEffect } from 'react';
import { changePasswordSchema } from './change-password-schema';
import {
    useAPI,
    useAPIHeader,
    useAxiosErrorHandler,
    useFormValidation
} from './../../../hooks';
import { AlertMessage, AlertType } from './../../../assets/js/alert';
import { hideLoading, showLoading } from './../../../assets/js/loading';
import Axios from 'axios';
import { useDispatch } from 'react-redux';
import { ModalActionType } from './.././../../redux/reducers/modal.reducer';

function FormChangePassword() {
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);

    const apiHeader = useAPIHeader();
    const APIEndPoint = useAPI();
    const [form, validateForm] = useFormValidation(changePasswordSchema);
    const handleAxiosError = useAxiosErrorHandler();
    const dispatch = useDispatch();

    async function onFormSubmit(evt) {
        evt.preventDefault();

        if (!validateForm())
            return setAutoValidate(true);

        const requestBody = {
            old_password: oldPassword,
            new_password: newPassword
        };

        showLoading();

        try {
            await Axios.post(APIEndPoint.CHANGE_PASSWORD, requestBody, {
                headers: apiHeader
            });

            new AlertMessage({
                message: 'Password have been changed.',
                alertType: AlertType.SUCCESS
            }).show();

            dispatch({
                type: ModalActionType.CLOSE_MODAL
            });

        } catch (error) {
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    useEffect(() => {
        if (!autoValidate) return;

        validateForm();

    }, [newPassword, oldPassword, confirmPassword, autoValidate]);

    return (
        <form ref={form} noValidate onSubmit={onFormSubmit}>
            <div className="form-group">
                <label>New Password</label>
                <input id="newPassword" type="password" className="form-control" value={newPassword} onChange={(evt) => setNewPassword(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <label>Confirm New Password</label>
                <input id="confirmPassword" type="password" className="form-control" value={confirmPassword} onChange={(evt) => setConfirmPassword(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <label>Your Old Password</label>
                <input id="oldPassword" type="password" className="form-control" value={oldPassword} onChange={(evt) => setOldPassword(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <button className="btn btn-danger float-right" type="submit">Done</button>
            </div>
        </form>
    );
}

export default FormChangePassword;