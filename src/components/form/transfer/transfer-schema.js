export const createTransferSchema = Object.freeze({
    transferAmount: {
        presence: {
            allowEmpty: false
        },
        numericality: true,
        currency: true
    },
    recieverAccount: {
        presence: {
            allowEmpty: false
        },
        numericality: true,
        currency: true
    }
});