import React, { useState, useEffect } from 'react';
import { 
    useAPI,
    useAPIHeader,
    useAxiosErrorHandler,
    useDollarToCentConverter,
    useFormValidation,
    useCentToDollarConverter
 } from './../../../hooks';
import { createTransferSchema } from './transfer-schema';
import Axios from 'axios';
import { AlertMessage, AlertType } from './../../../assets/js/alert';
import { showLoading, hideLoading } from './../../../assets/js/loading';
import $ from 'jquery';
import { useDispatch } from 'react-redux';
import { ModalActionType } from './../../../redux/reducers/modal.reducer';
import { TransactionActionType } from './../../../redux/reducers/transaction.reducer';

function FormTransfer() {
    const [transferAmount, setTransferAmount] = useState('');
    const [recieverAccount, setRecieverAccount] = useState('');
    const [remark ,setRemark] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);

    const [recieverAccountName, setRecieverAccountName] = useState('N/A');

    const [transferForm, validateTransferForm] = useFormValidation(createTransferSchema);
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();
    const dispatch = useDispatch();
    const dollarToCent = useDollarToCentConverter();

    async function formTransferSubmit(evt) {
        evt.preventDefault();

        if (!validateTransferForm())
            return setAutoValidate(true);

        showLoading();
        // get reciever info
        try {
            const response = await Axios.get(APIEndPoint.GET_ACCOUNT, {
                params: {
                    account_id: recieverAccount
                },
                headers: apiHeader
            });
            
            const account = response.data.data;

            if (!account)
                return new AlertMessage({
                    message: 'Reciever account does not exist',
                    alertType: AlertType.WARNING
                }).show();

            // update UI of form confirm
            
            setRecieverAccountName(account.account_name);

            $('.tab-pane').removeClass('show active');
            $('#tabConfirm').tab('show');

        } catch (error) {
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    async function confirmTransferSubmit(evt) {
        evt.preventDefault();

        const requestBody = {
            transfer_amount: dollarToCent(transferAmount),
            transfer_to_account: parseInt(recieverAccount)
        };

        if (remark)
            requestBody.transfer_remark = remark;

        showLoading();

        try {
            await Axios.post(APIEndPoint.TRANSFER, requestBody, {
                headers: apiHeader
            });
            new AlertMessage({
                message: `You have transfered USD${transferAmount} to account ${recieverAccountName}`,
                alertType: AlertType.SUCCESS
            }).show();

            dispatch({
                type: TransactionActionType.HAS_TRANSACTION,
                payload: {
                    transactionBalance: -1 * dollarToCent(transferAmount)
                }
            });

            // close form
            dispatch({
                type: ModalActionType.CLOSE_MODAL
            });

        } catch (error) {
            console.log(error.response.data);
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    function cancelTransfer() {
        // close form
        dispatch({
            type: ModalActionType.CLOSE_MODAL
        });
    }

    useEffect(() => {
        if (!autoValidate) return;

        validateTransferForm();

    }, [transferAmount, recieverAccount, remark, autoValidate]);

    return (
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="tabTransfer" role="tabpanel" aria-labelledby="pills-home-tab">
                <form ref={transferForm} onSubmit={formTransferSubmit} noValidate>
                    <div className="form-group">
                        <label htmlFor="transferAmount">Transfer Amount</label>
                        <input id="transferAmount" type="text" className="form-control" value={transferAmount} onChange={(evt) => setTransferAmount(evt.currentTarget.value)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="recieverAccountNumber">Reciever Account Number</label>
                        <input id="recieverAccount" type="text" className="form-control" value={recieverAccount} onChange={(evt) => setRecieverAccount(evt.currentTarget.value)} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="remark">Remark</label>
                        <textarea id="remark" type="text" className="form-control" style={{ minHeight: '50px' }} value={remark} onChange={(evt) => setRemark(evt.currentTarget.value)} />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-danger float-right">Done</button>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="tabConfirm" role="tabpanel" aria-labelledby="pills-profile-tab">
                <form onSubmit={confirmTransferSubmit} noValidate>
                    <h4 className="text-center">Reciever Account Information</h4>
                    <div className="form-group">
                        <label>Account Name</label>
                        <input type="text" className="form-control" value={recieverAccountName} disabled />
                    </div>
                    <div className="form-group d-flex flex-column justify-content-center align-items-center">
                        <h6 className="text-warning">Are you sure to transfer to this account ?</h6>
                        <div className="btn-group">
                            <button type="submit" className="btn btn-primary">Confirm</button>
                            <button type="button" className="btn btn-warning" onClick={(evt) => cancelTransfer()}>Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default FormTransfer;