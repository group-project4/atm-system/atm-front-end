import React from 'react';
import { 
    useAPIHeader,
    useAPI,
    useAxiosErrorHandler,
    useFormValidation,
    useCentToDollarConverter,
    useDollarToCentConverter
} from './../../../hooks/index';
import { useState } from 'react';
import { useEffect } from 'react';
import { showLoading, hideLoading } from './../../../assets/js/loading';
import { AlertMessage, AlertType } from './../../../assets/js/alert';
import Axios from 'axios';
import { useDispatch } from 'react-redux';
import { ModalActionType } from './../../../redux/reducers/modal.reducer';
import { TransactionActionType } from './../../../redux/reducers/transaction.reducer';
import { createWithdrawalSchemal, createWithdrawalSchema } from './withdrawal-schema';

function FormWithdrawal() {
    const [withdrawalAmount, setWithdrawalAmount] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);

    const [form, validateForm] = useFormValidation(createWithdrawalSchema);
    const dollarToCent = useDollarToCentConverter();
    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const dispatch = useDispatch();
    const handleAxiosError = useAxiosErrorHandler();

    async function onFormSubmit(evt) {
        evt.preventDefault();

        if (!validateForm())
            return setAutoValidate(true);

        const requestBody = {
            withdraw_amount: dollarToCent(withdrawalAmount)
        };

        showLoading();

        try {
            await Axios.post(APIEndPoint.WITHDRAW, requestBody, {
                headers: apiHeader
            });
            new AlertMessage({
                message: `Your have withdrew USD ${withdrawalAmount} from your account.`,
                alertType: AlertType.SUCCESS
            }).show();

            dispatch({
                type: TransactionActionType.HAS_TRANSACTION,
                payload: {
                    transactionBalance: -1 * dollarToCent(withdrawalAmount)
                }
            });

            dispatch({
                type: ModalActionType.CLOSE_MODAL,
                payload: {}
            });

        } catch (error) {
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    useEffect(() => {
        if (!autoValidate)  return;
        
        validateForm();
        
    }, [withdrawalAmount, autoValidate]);

    return (
        <form ref={form} onSubmit={onFormSubmit} noValidate>
            <div className="form-group">
                <label>Withdrawal Amount</label>
                <input id="withdrawalAmount" type="text" className="form-control" value={withdrawalAmount} onChange={(evt) => setWithdrawalAmount(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right">Done</button>
            </div>
        </form>
    );
}

export default FormWithdrawal;