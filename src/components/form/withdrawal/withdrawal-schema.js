export const createWithdrawalSchema = Object.freeze({
    withdrawalAmount: {
        presence: {
            allowEmpty: false
        },
        numericality: true,
        currency: true
    }
});