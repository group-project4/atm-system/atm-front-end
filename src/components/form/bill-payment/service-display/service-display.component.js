import React from 'react';
import './service-display.style.css';

function ServiceDisplay(props) {
    return (
        <div className="service-display d-flex align-items-center">
            <img className="service-display-icon" src={props.serviceIcon} alt="" />
            <span className="service-name ml-3">{props.serviceName}</span>
        </div>
    );
}

export default ServiceDisplay;