import React, { useEffect, useState } from 'react';
import ProviderDisplay from '../provider-display/provider-display.component';
import { useDispatch } from 'react-redux';
import { ModalActionType } from '../../../../redux/reducers/modal.reducer';
import FormBillInput from '../bill-input/form-bill-input.component';
import { showLoading, hideLoading } from '../../../../assets/js/loading';
import Axios from 'axios';
import {  
    useAPI,
    useAPIHeader,
    useAxiosErrorHandler,
    useConvertToAPIFile
} from '../../../../hooks';

function FormShowBiller({billerTypeId}) {
    const [billers, setBillers] = useState([]);
    const [noBillerAvailableMessage, setNoBillerAvailableMessage] = useState('');

    const dispatch = useDispatch();
    const apiHeader = useAPIHeader();
    const APIEndPoint = useAPI();
    const hanldeAxiosError = useAxiosErrorHandler();
    const convertToAPIFile = useConvertToAPIFile();

    function showBillerInputForm(billerId) {
        dispatch({
            type: ModalActionType.OPEN_MODAL,
            payload: {
                title: 'Billing',
                form: <FormBillInput billerId={billerId} />
            }
        });
    }

    function getBillers(billerTypeId) {
        showLoading();
        Axios.get(`${APIEndPoint.GET_BILLER_BY_BILLER_TYPE}/${billerTypeId}`, {
            headers: apiHeader
        })
        .then((response) => response.data)
        .then((jsonResponse) => jsonResponse.data.length === 0 ? setNoBillerAvailableMessage('No Billers available for this service') : setBillers(jsonResponse.data))
        .catch((error) => hanldeAxiosError(error))
        .finally(() => hideLoading());
    }

    useEffect(() => {
        getBillers(billerTypeId);
    }, [billerTypeId]);

    return (
        <form>
            <div className="form-group">
                <label>Chhose Billers</label>
                <div id="biller" className="p-2 border">
                    {billers.map((biller) => {
                        return (
                            <div className="my-1" key={biller.biller_id} onClick={(evt) => showBillerInputForm(biller.biller_id)}>
                                <ProviderDisplay providerName={biller.biller_name} providerLogo={convertToAPIFile(biller.biller_logo)} />
                            </div>
                        )
                    })}
                    <h5 className="text-danger text-center">{noBillerAvailableMessage}</h5>
                </div>
            </div>
        </form>
    );
}

export default FormShowBiller;