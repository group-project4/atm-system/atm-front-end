import React, { useState, useEffect } from 'react';
import {
    useAPI,
    useAPIHeader,
    useAxiosErrorHandler,
    useFormValidation,
    useDollarToCentConverter
} from './../../../../hooks';
import { createBillingSchema } from './../bill-payment-schema';
import { showLoading, hideLoading } from '../../../../assets/js/loading';
import { AlertMessage, AlertType } from './../../../../assets/js/alert';
import Axios from 'axios';
import { useDispatch } from 'react-redux';
import { ModalActionType } from '../../../../redux/reducers/modal.reducer';
import { TransactionActionType } from './../../../../redux/reducers/transaction.reducer';

function FormBillInput({billerId}) {
    const [billingAccountNumber, setBillingAccountNumber] = useState('');
    const [amount, setAmount] = useState('');
    const [remark, setRemark] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);

    const apiHeader = useAPIHeader();
    const APIEndPoint = useAPI();
    const handleAxiosError = useAxiosErrorHandler();
    const [form, validateForm] = useFormValidation(createBillingSchema);
    const dollarToCent = useDollarToCentConverter();
    const dispatch = useDispatch();

    async function onSubmit(evt) {
        evt.preventDefault();
        
        if (!validateForm()) return setAutoValidate(true);

        const requestBody = {
            biller_id: billerId,
            billing_amount: dollarToCent(amount),
            billing_account_number: billingAccountNumber
        }

        if (remark) requestBody.remark = remark;

        showLoading();

        try {
            await Axios.post(APIEndPoint.PAY_BILL, requestBody, {
                headers: apiHeader
            });

            new AlertMessage({
                message: 'Your billing has been successfully processed',
                alertType: AlertType.SUCCESS
            }).show();

            dispatch({
                type: TransactionActionType.HAS_TRANSACTION,
                payload: {
                    transactionBalance: -1 * dollarToCent(amount)
                }
            });

            dispatch({
                type: ModalActionType.CLOSE_MODAL
            });

        } catch (error) {
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    useEffect(() => {
        if (!autoValidate) return;

        validateForm();

    }, [billingAccountNumber, amount, remark, autoValidate])

    return (
        <form ref={form} onSubmit={onSubmit}>
            <div className="form-group">
                <label>Billing Account Number</label>
                <input id="billingAccountNumber" type="text" className="form-control" value={billingAccountNumber} onChange={(evt) => setBillingAccountNumber(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <label>Amount</label>
                <input id="amount" type="text" className="form-control" value={amount} onChange={(evt) => setAmount(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <label>Remark</label>
                <textarea id="remark" className="form-control" style={{ minHeight: '50px' }} value={remark} onChange={(evt) => setRemark(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right">Done</button>
            </div>
        </form>
    );
}

export default FormBillInput;