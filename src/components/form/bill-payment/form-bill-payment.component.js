import React, { useState, useEffect } from 'react';
import ServiceDisplay from './service-display/service-display.component';
import { useDispatch } from 'react-redux';
import { ModalActionType } from '../../../redux/reducers/modal.reducer';
import FormShowBiller from './show-biller/form-show-biller.component';
import Axios from 'axios';
import {showLoading, hideLoading } from './../../../assets/js/loading';
import {
    useDollarToCentConverter,
    useAPIHeader,
    useAPI,
    useAxiosErrorHandler
} from './../../../hooks';

function FormBillPayment() {
    const [services, setServices] = useState([]); 
    const dispatch = useDispatch();

    const apiHeader = useAPIHeader();
    const APIEndPoint = useAPI();
    const dollarToCent = useDollarToCentConverter();
    const handleAxiosError = useAxiosErrorHandler();

    function getAllServices() {
        showLoading();
        Axios.get(APIEndPoint.GET_BILLER_TYPES, {
            headers: apiHeader
        })
        .then((response) => {
            return response.data;
        })
        .then((jsonResponse) => {
            setServices(jsonResponse.data);
        })
        .catch((error) => {
            handleAxiosError(error);
        })
        .finally(() => {
            hideLoading();
        });
    }

    function showFormBiller(billerTypeId) {
        // show biller base on biller type
        dispatch({
            type: ModalActionType.OPEN_MODAL,
            payload: {
                title: 'Biller',
                form: <FormShowBiller billerTypeId={billerTypeId} />
            }
        });
    }

    useEffect(() => {
        getAllServices();
    }, []);

    return (
        <form>
            <div className="">
                <label>Choose Sevices</label>
                <div id="serviceType" className="border p-2">
                    {services.map((service) => {
                        return (
                            <div key={service.biller_type_id} className="my-1" onClick={(evt) => showFormBiller(service.biller_type_id)}>
                                <ServiceDisplay serviceName={service.biller_type_name} />
                            </div>
                        )
                    })}
                </div>
            </div>
        </form>
    );
}

export default FormBillPayment;