export const createBillingSchema = Object.freeze({
    billingAccountNumber: {
        presence: {
            allowEmpty: false
        }
    },
    billingAccountNumber: {
        presence: {
            allowEmpty: false
        }
    }, 
    amount: {
        presence: {
            allowEmpty: false
        },
        numericality: true,
        currency: true
    }
});