import React from 'react';
import './provider-display.style.css';

function ProviderDisplay(props) {
    return (
        <div className="provider-display d-flex align-items-center">
            <img className="provider-logo border" src={props.providerLogo} alt="" />
            <span className="provider-name ml-2 font-weight-bold">{props.providerName}</span>
        </div>
    );
}

export default ProviderDisplay;