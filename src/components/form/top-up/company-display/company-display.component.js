import React from 'react';
import './company-display.style.css';

function CompanyDisplay(props) {
    return (
        <div className="company-container d-inline-flex flex-column mx-2 my-3 p-2 border">
            <img className="company-icon" src={props.icon} alt="company-icon" />
            <span className="d-block text-center company-title">{props.company}</span>
        </div>
    );
}

export default CompanyDisplay;