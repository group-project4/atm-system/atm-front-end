export const createTopupSchema = Object.freeze({
    topupBalance: {
        presence: {
            allowEmpty: false,
            message: '^Please select a balance option.'
        }
    },
    billerId: {
        presence: {
            allowEmpty: false,
            message: '^Please select a company.'
        }
    },
    phoneNumber: {
        presence: {
            allowEmpty: false,
        },
        format: {
            pattern: '^\\d{9,15}$',
            message: '^Not a phone number format.'
        }
    }
});