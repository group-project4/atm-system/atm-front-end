import React, { useState, useRef, useEffect } from 'react';
import CompanyDisplay from './company-display/company-display.component';
import cellCardIcon from './../../../assets/icon/unnamed.png'
import { useSelector, useDispatch } from 'react-redux';
import { ModalActionType } from '../../../redux/reducers/modal.reducer';
import { transactionReducer, TransactionActionType } from '../../../redux/reducers/transaction.reducer';
import BalanceDisplay from './balance-display/balance-display.component';
import {  
    useAPI,
    useAPIHeader,
    useAxiosErrorHandler,
    useDollarToCentConverter,
    useFormValidation,
    useConvertToAPIFile    
} from './../../../hooks';
import { AlertMessage, AlertType } from './../../../assets/js/alert';
import { hideLoading, showLoading } from './../../../assets/js/loading';
import Axios from 'axios';
import { createTopupSchema } from './topup-schema';

function FormTopUp() {
    const [validBalance, setValidBalance] = useState([1, 2, 5, 10, 20, 50]);
    const [phoneNumber, setPhoneNumber] = useState('');
    const [topupBalance, setTopupBalance] = useState('');
    const [remark, setRemark] = useState('');
    const [autoValidate, setAutoValidate] = useState(false);
    const [billerId, setBillerId] = useState('');
    const [billers, setBillers] = useState([]);
    const [noCompanyAvailableMessage, setNoCompanyAvaiableMessage] = useState('');

    const APIEndPoint = useAPI();
    const apiHeader = useAPIHeader();
    const handleAxiosError = useAxiosErrorHandler();
    const [form, validateForm] = useFormValidation(createTopupSchema);
    const dollarToCent = useDollarToCentConverter();
    const convertToAPIFile = useConvertToAPIFile();

    const dispatch = useDispatch();
    const balanceContainerRef = useRef();
    const companyContainerRef = useRef();

    function getTelecomeBiller() {
        showLoading();
        Axios.get(`${APIEndPoint.GET_BILLER_BY_BILLER_NAME}/telecome`, {
            headers: apiHeader
        })
        .then((response) => response.data)
        .then((jsonRespose) => jsonRespose.data.length === 0 ? setNoCompanyAvaiableMessage('No company available for now.') : setBillers(jsonRespose.data))
        .catch((error) => handleAxiosError(error))
        .finally(() => hideLoading());
    }

    async function onSubmit(evt) {
        evt.preventDefault();

        if (!validateForm())
            return setAutoValidate(true);

        const requestBody = {
            topup_amount: dollarToCent(topupBalance),
            biller_id: billerId,
            phone_number: phoneNumber
        };  

        if (remark) requestBody.topup_remark = remark

        showLoading();
        
        try {
            await Axios.post(APIEndPoint.TOPUP, requestBody, {
                headers: apiHeader
            });

            new AlertMessage({
                message: `You have topup USD${topupBalance} to ${phoneNumber}`,
                alertType: AlertType.SUCCESS
            }).show();

            dispatch({
                type: TransactionActionType.HAS_TRANSACTION,
                payload: {
                    transactionBalance: -1 * dollarToCent(topupBalance)
                }
            })

            dispatch({
                type: ModalActionType.CLOSE_MODAL
            });

        } catch (error) {
            handleAxiosError(error);
        } finally {
            hideLoading();
        }
    }

    function selectCompany(currentElement, companyId) {
        const companyContainerElement = currentElement.children[0];
        companyContainerElement.style.boxShadow = '0 0 0 0.2rem rgba(225,83,97,.8)';
        // remove other company style
        for (const child of companyContainerRef.current.querySelectorAll('.biller')) {
            const otherCompanyContainer = child.children[0]; 
            if (otherCompanyContainer !== companyContainerElement) {
                otherCompanyContainer.style.boxShadow = '';
            }
        }
        setBillerId(companyId);
    }

    function selectBalance(currentElement, balance) {
        const balanceContainerElement = currentElement.children[0];
        balanceContainerElement.style.boxShadow = '0 0 0 0.2rem rgba(225,83,97,.8)';
        // remove other balance style
        for (const child of balanceContainerRef.current.children) {
            const otherBalanceContainer = child.children[0]; 
            if (otherBalanceContainer !== balanceContainerElement) {
                otherBalanceContainer.style.boxShadow = '';
            }
        }
        setTopupBalance(balance);
    }

    useEffect(() => {
        getTelecomeBiller(); 
    }, []);

    useEffect(() => {
        if (!autoValidate) return;

        validateForm();

    }, [phoneNumber, topupBalance, billerId, autoValidate]);

    return (
        <form ref={form} noValidate onSubmit={onSubmit}>
            <div className="form-group">
                <label htmlFor="phoneNumber">Phone Number</label>
                <input id="phoneNumber" type="text" className="form-control" value={phoneNumber} onChange={(evt) => setPhoneNumber(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <label>Balance</label>
                <div id="balance" className="d-flex justify-content-center border" ref={balanceContainerRef}>
                    {validBalance.map((balance) => {
                        return (
                            <div key={balance} onClick={(evt) => selectBalance(evt.currentTarget, balance)}>
                                <BalanceDisplay balance={balance} />
                            </div>
                        )
                    })}
                </div>
                <input id="topupBalance" type="hidden" value={topupBalance} />
            </div>
            <div className="form-group">
                <label>Company</label>
                <div id="company" className="d-flex justify-content-center border" ref={companyContainerRef}>
                    {billers.map((biller) => {
                        return (
                            <div key={biller.biller_id} onClick={(evt) => selectCompany(evt.currentTarget, biller.biller_id)} className="biller" >
                                <CompanyDisplay company={biller.biller_name} icon={convertToAPIFile(biller.biller_logo)} />
                            </div>
                        )
                    })}
                    <h5 className={`text-danger ${noCompanyAvailableMessage !== '' ? 'my-3' : ''}`}>{noCompanyAvailableMessage}</h5>
                </div>
                <input id="billerId" value={billerId} type="hidden" />
            </div>
            <div className="form-group">
                <label>Remark</label>
                <textarea id="remark" className="form-control" style={{ minHeight: '50px' }} value={remark} onChange={(evt) => setRemark(evt.currentTarget.value)} />
            </div>
            <div className="form-group">
                <button type="submit" className="btn btn-danger float-right" disabled={billers.length === 0}>Done</button>
            </div>
        </form>
    );
}

export default FormTopUp;