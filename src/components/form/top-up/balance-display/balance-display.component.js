import React from 'react';
import './balance-display.style.css';

function BalanceDisplay(props) {
    return (
        <div className="balance-container d-inline-flex flex-column mx-2 my-3 p-2 border">
            <span className="balance d-inline-flex align-items-center justify-content-center font-weight-bold">${props.balance}</span>
        </div>
    );
}

export default BalanceDisplay