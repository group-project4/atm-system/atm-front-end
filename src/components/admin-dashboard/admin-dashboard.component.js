import React from 'react';
import { Switch, Route, NavLink, Redirect, useHistory } from 'react-router-dom';
import FormUser from '../admin-form/form-user/form-user.component';
import './admin-dashboard.style.css';
import FormBiller from '../admin-form/form-biller/form-biller.component';
import FormService from '../admin-form/form-service/form-service.component';
import FormBilling from '../admin-form/form-billing/form-billing.component';
import FormDeposit from '../admin-form/form-deposit/form-deposit.component';
import FormWithdraw from '../admin-form/form-withdrawal/form-withdrawal';
import { useLogout } from '../../hooks/logout.hook';
import FormTransfer from '../admin-form/form-transfer/form-transfer.component';
import FormTopup from '../admin-form/form-topup/form-topup.component';

function AdminDashboard() {
    const logout = useLogout();
    return (
        <div id="adminDashboard" className="d-flex" style={{height: '100vh'}}>
            <aside className="bg-dark shadow-lg position-relative" style={{ width: '250px', zIndex: '10' }}>
                <NavLink className="menu-item" to="/admin/user">User</NavLink>
                <NavLink className="menu-item" to="/admin/biller">Biller</NavLink>
                <NavLink className="menu-item" to="/admin/service">Service</NavLink>
                <NavLink className="menu-item" to="/admin/billing">Billing Transaction</NavLink>
                <NavLink className="menu-item" to="/admin/deposit">Deposit Transaction</NavLink>
                <NavLink className="menu-item" to="/admin/withdraw">Withdrawal Transaction</NavLink>
                <NavLink className="menu-item" to="/admin/transfer">Transfer Transaction</NavLink>
                <NavLink className="menu-item" to="/admin/topup">Topup Transaction</NavLink>
                <NavLink className="menu-item text-danger" to="/admin/logout">Logout</NavLink>
            </aside>
            <div className="flex-grow-1 bg-light p-3" style={{'overflowX': 'unset', 'overflowY': 'auto'}}>
                <Switch>
                    <Route path="/admin/user" component={FormUser} />
                    <Route path="/admin/biller" component={FormBiller} />
                    <Route path="/admin/service" component={FormService} />
                    <Route path="/admin/billing" component={FormBilling} />
                    <Route path="/admin/deposit" component={FormDeposit} />
                    <Route path="/admin/withdraw" component={FormWithdraw} />
                    <Route path="/admin/transfer" component={FormTransfer} />
                    <Route path="/admin/topup" component={FormTopup} />
                    <Route path="/admin/logout" render={() => {
                        logout();
                        return <Redirect to="/login" />
                    }} />
                    <Route path="/admin" render={() => <Redirect to="/admin/user" />} />
                </Switch>
            </div>
        </div>
    );
}

export default AdminDashboard;